import os
import urllib.request
from bs4 import BeautifulSoup

articles_to_scrape = 30
html_path = "./html_files/"


def get_urls_of_articles():
    return [f"https://dsgvo-gesetz.de/art-{i + 1}-dsgvo/" for i in range(articles_to_scrape)]


def get_html_files(i, urls):
    filename = "article" + str(i + 1) + ".html"
    path_to_file = os.path.join(html_path, filename)
    if not os.path.exists(path_to_file):
        urllib.request.urlretrieve(urls[i], path_to_file)
    return filename


def get_reason_for_article(article_number, filename):
    with open(html_path + filename, "r") as html_file:
        contents = html_file.read()
        soup = BeautifulSoup(contents, 'html.parser')
        output = "Artikel " + str(article_number) + ": "
        for reason in soup.find_all(class_="bold-number"):
            if "BDSG" not in reason.get_text():
                output += ("§" + reason.get_text() + ", ")
        if output == "Artikel " + str(article_number) + ": ":
            output = "Artikel " + str(article_number) + " hat keine Erwägungsgründe."
        print(output.rstrip(", "))
        return output


def get_reasons_for_dsgvo():
    urls = get_urls_of_articles()
    reasons = []
    for j in range(0, articles_to_scrape):
        filename = get_html_files(j, urls)
        get_reason_for_article(j + 1, filename)


if __name__ == '__main__':
    get_reasons_for_dsgvo()
